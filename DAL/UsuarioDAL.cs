﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;


namespace DAL
{
    public class UsuarioDAL
    {
        public static Usuarios ConsultarUsuario(int rut)
        {
            
            Usuarios user = new Usuarios();
            
                using (SUBASTASEntities bd = new SUBASTASEntities())
                {
                    var query = (from us in bd.Usuarios
                                 where us.rut_usuario == rut
                                 select us).Single();

                    user.rut_usuario = query.rut_usuario;
                    user.nombre_usuario = query.nombre_usuario;
                    user.dv_usuario = query.dv_usuario;
                    user.tipo_usuario = query.tipo_usuario;
                    user.apellido_usuario = query.apellido_usuario;
                    user.password = query.password;
                }
            
           
            return user;

        }

        public static Usuarios RegistrarUsuario(Usuarios px)
        {

            using (SUBASTASEntities bd = new SUBASTASEntities())
            {

                Usuarios p = new Usuarios();
                p.rut_usuario = px.rut_usuario;
                p.dv_usuario = px.dv_usuario;
                p.nombre_usuario = px.nombre_usuario;
                p.apellido_usuario = px.apellido_usuario;
                p.password = px.password;
                p.tipo_usuario = px.tipo_usuario;
                bd.Usuarios.Add(p);
                bd.SaveChanges();


            }
            return px;

        }

    }
}
