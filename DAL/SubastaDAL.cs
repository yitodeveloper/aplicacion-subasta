﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace DAL
{
    public class SubastaDAL
    {
        public static Subastas SubastarProducto(Subastas px)
        {

            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                Subastas p = new Subastas();
                p.id_producto = px.id_producto;
                p.rut_usuario = px.rut_usuario;
                p.fecha_subasta = px.fecha_subasta;
                p.monto_subastado = px.monto_subastado;
                bd.Subastas.Add(p);
                bd.SaveChanges();
                px.id_subasta = p.id_subasta;

            }
            return px;

        }

        public static List<Subastas> CargarSubastasProducto(int id_producto)
        {
            
            List<Subastas> pr = new List<Subastas>();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var query = from p in bd.Subastas
                            where p.id_producto == id_producto
                            select p;

                pr = query.ToList();
            }
            return pr;

        }

        public static Subastas CargarUltimaSubasta(int id_producto, int precioactual)
        {

            Subastas pr = new Subastas();
            try
            {
                using (SUBASTASEntities bd = new SUBASTASEntities())
                {
                    var query = (from p in bd.Subastas
                                 where p.id_producto == id_producto && p.monto_subastado == precioactual
                                 select p).Single();

                    pr = query;
                }
            }
            catch (Exception ed) { }
            return pr;

        }

    }
}
