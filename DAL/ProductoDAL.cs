﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace DAL
{
    public class ProductoDAL
    {
        public static List<Producto> CargarProductosDisponibles()
        {
            String estado = "Disponible";
            List<Producto> pr = new List<Producto>();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var query = from p in bd.Producto
                            where p.estado == estado
                            select p;

                pr = query.ToList();
            }
            return pr;

        }

        public static List<Producto> CargarProductosVendidos()
        {
            String estado = "Vendido";
            List<Producto> pr = new List<Producto>();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var query = from p in bd.Producto
                            where p.estado == estado
                            select p;

                pr = query.ToList();
            }
            return pr;

        }

        public static List<Producto> CargarProductosDisponiblesFiltro(String q)
        {
            String estado = "Disponible";
            List<Producto> pr = new List<Producto>();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var query = from p in bd.Producto
                            where p.estado == estado && p.nombre_producto.StartsWith(q)
                            select p;

                pr = query.ToList();
            }
            return pr;

        }

        public static List<Producto> CargarProductosUsuario(int rut)
        {
            List<Producto> pr = new List<Producto>();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var query = from p in bd.Producto
                            where p.rut_usuario == rut 
                            select p;

                pr = query.ToList();
            }
            return pr;

        }


        public static Producto ActualizarEstadoProducto(Producto px, String estado)
        {
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var p = (from pr in bd.Producto
                         where pr.id_producto == px.id_producto
                         select pr).Single();

                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.fecha_inscripcion = p.fecha_inscripcion;
                p.estado = estado;
                bd.SaveChanges();
            }
            return px;
        }

        public static Producto RegistrarProducto(Producto px)
        {

            using (SUBASTASEntities bd = new SUBASTASEntities())
            {

                Producto p = new Producto();
                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.fecha_inscripcion = px.fecha_inscripcion;
                p.estado = px.estado;
                bd.Producto.Add(p);
                bd.SaveChanges();

                px.id_producto = p.id_producto;

            }
            return px;

        }


        public static Producto RepublicarProducto(Producto px)
        {
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var p = (from pr in bd.Producto
                         where pr.id_producto == px.id_producto
                         select pr).Single();

                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.fecha_inscripcion = px.fecha_inscripcion;
                p.estado = px.estado;
                bd.SaveChanges();
            }
            return px;
        }

        public static Producto UpdateProducto(Producto px)
        {
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var p = (from pr in bd.Producto
                         where pr.id_producto == px.id_producto
                         select pr).Single();

                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.precio_final = px.precio_final;
                p.fecha_inscripcion = p.fecha_inscripcion;
                p.estado = px.estado;
                bd.SaveChanges();
            }
            return px;
        }

        public static Producto ComprarProducto(Producto px)
        {
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var p = (from pr in bd.Producto
                         where pr.id_producto == px.id_producto
                         select pr).Single();

                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.precio_final = px.precio_final;
                p.fecha_inscripcion = px.fecha_inscripcion;
                p.estado = px.estado;
                bd.SaveChanges();
            }
            return px;
        }


        public static Producto ConsultarProducto(int id_producto)
        {
            Producto p = new Producto();
            using (SUBASTASEntities bd = new SUBASTASEntities())
            {
                var px = (from pr in bd.Producto
                             where pr.id_producto == id_producto
                             select pr).Single();

                p.id_producto = px.id_producto;
                p.rut_usuario = px.rut_usuario;
                p.nombre_producto = px.nombre_producto;
                p.descripcion_producto = px.descripcion_producto;
                p.url_img = px.url_img;
                p.precio_inicial = px.precio_inicial;
                p.precio_final = px.precio_final;
                p.fecha_inscripcion = px.fecha_inscripcion;
                p.estado = px.estado;
            }
            return p;
        }
    }
}
