﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using BLL;
using System.IO;

namespace GUI
{
    public partial class Vendedor : Form
    {
        public Vendedor()
        {   
            InitializeComponent();
            btnModificar.Enabled = false;
            lblidproducto.Text = "";
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void AsignarUsuario(String user) {
            lblUsuario.Text = user;
            ListarProductosUsuario(0);
        }

        private void btnCargarImagen_Click(object sender, EventArgs e)
        {
            OpenFileDialog n = new OpenFileDialog();

            n.Filter = "Archivo de Imagenes|*.png;*.jpeg;*.jpg";

            if (n.ShowDialog() == DialogResult.OK) {
                pictureBoxImagen.ImageLocation = n.FileName;
            }
        }

        private void btnVender_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario()) {
                registrarProducto();
                limpiarForm();
            }
        }


        private void registrarProducto() {

            Producto p = new Producto();
            p.rut_usuario = Convert.ToInt32(lblUsuario.Text);
            p.nombre_producto = txtNombre.Text;
            p.descripcion_producto = txtDescripcion.Text;
            p.precio_inicial = Convert.ToInt32(txtPrecio.Text);
            p.url_img = pictureBoxImagen.ImageLocation;
            p.fecha_inscripcion = Convert.ToString(DateTime.Now);
            p.estado = "Disponible";

            if (BLL.ProductosBLL.VenderProducto(p).id_producto > 0)
            {

                MessageBox.Show("Articulo registrado exitosamente, exito en la subasta");
                
            }
            else {
                MessageBox.Show("Error no se pudo registrar producto");
            }
        }

        private void limpiarForm() {
            lblidproducto.Text = "";
            txtDescripcion.Text = "";
            txtNombre.Text = "";
            txtPrecio.Text = "";
            btnModificar.Enabled = false;
            btnVender.Enabled = true;
            txtDescripcion.ReadOnly = false;
            txtNombre.ReadOnly = false;
            txtPrecio.ReadOnly = false;
            pictureBoxImagen.Image = GUI.Properties.Resources.icon320x320;
        
        }
        private Boolean ValidarFormulario() {

            if (
                txtDescripcion.Text != "" &&
                txtNombre.Text != "" &&
                txtPrecio.Text != ""
                )
            {

                return true;
            }
            else {

                MessageBox.Show("Datos Incompletos, no se puede realizar operación");
                return false;
            
            }
        
        
        
        }


        private void ListarProductosUsuario(int filaseleccionada)
        {
            
            DataTable tabla = new DataTable();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("DESCRIPCIÓN");
            tabla.Columns.Add("ULTIMA OFERTA ($)");
            tabla.Columns.Add("TIEMPO RESTANTE");
            tabla.Columns.Add("ESTADO");

            List<Producto> resultado = ProductosBLL.CargarProductosUsuario(Convert.ToInt32(lblUsuario.Text));

            if (resultado.Count > 0)
            {

                foreach (Producto p in resultado)
                {

                    tabla.Rows.Add(p.id_producto , p.nombre_producto, p.descripcion_producto, p.precio_final, Cronometro(p.fecha_inscripcion, p),p.estado);
                }

            }

            try
            {
                GrillaProductos.DataSource = tabla;
                GrillaProductos.CurrentRow.Selected = false;
                GrillaProductos.Rows[filaseleccionada].Selected = true;
                GrillaProductos.Columns[0].Width = 50;
                GrillaProductos.Columns[1].Width = 250;
                GrillaProductos.Columns[2].Width = 320;
            }
            catch (NullReferenceException ne) { 
            
            }
        }

        private void Vendedor_Load(object sender, EventArgs e)
        {

        }

        private void Reloj1_Tick(object sender, EventArgs e)
        {
            int filaseleccionada = 0;
            try
            {
                filaseleccionada = GrillaProductos.SelectedRows[0].Index;
            }
            catch (Exception ed) { }
            ListarProductosUsuario(filaseleccionada);
            
        }

        private String Cronometro(String tiempo, Producto p)
        {
            if (p.estado.Equals("Disponible"))
            {
                StreamReader ficheroLectura = new StreamReader(@"C:\Users\Rodrigo\documents\visual studio 2010\Projects\SubastAPP\GUI\minutos.txt");
                int lineaFicheroL = Convert.ToInt32(ficheroLectura.ReadLine());
                ficheroLectura.Close();


                int minutos = lineaFicheroL;
                int segundos = 60;
                DateTime fechaAntiguo = Convert.ToDateTime(tiempo);
                DateTime fechaNueva = DateTime.Now;

                TimeSpan diferencia = fechaNueva - fechaAntiguo;

                int dfMinutos = minutos - diferencia.Minutes;
                int dfSegundos = segundos - diferencia.Seconds;



                if (dfMinutos <= 0)
                {

                    return (Convert.ToString(dfSegundos) + " segundos");


                }
                else
                {
                    return (Convert.ToString(dfMinutos) + ":" + Convert.ToString(dfSegundos));

                }


            }
            else {

                return ("expirado");
            }
            
        }

        private void GrillaProductos_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                CargarFormulario();
            }
            catch (InvalidCastException) { 
            
            }
        }

        private void CargarFormulario() {
            int idproducto = Convert.ToInt32(GrillaProductos.SelectedRows[0].Cells[0].Value);

            Producto p = BLL.ProductosBLL.ConsultarProducto(idproducto);

            if(p!=null)
            {

                if (p.precio_final==null)
                {

                    txtPrecio.Text = Convert.ToString(p.precio_inicial);
                    txtNombre.Text = p.nombre_producto;
                    txtDescripcion.Text = p.descripcion_producto;
                    
                    if (p.url_img.Trim().Equals(""))
                    {
                        pictureBoxImagen.Image = GUI.Properties.Resources.icon320x320;

                    }
                    else {
                        pictureBoxImagen.ImageLocation = p.url_img;
                    }
                    lblidproducto.Text = Convert.ToString(p.id_producto);

                    txtNombre.ReadOnly = true;
                    txtDescripcion.ReadOnly = true;
                    txtPrecio.ReadOnly = false;
                    btnVender.Enabled = false;
                    btnModificar.Enabled = true;


                }
                else {

                    MessageBox.Show("Articulo no se puede editar, debido a que ha sido subastado por un usuario");
                    txtPrecio.Text = Convert.ToString(p.precio_inicial);
                    txtNombre.Text = p.nombre_producto;
                    txtDescripcion.Text = p.descripcion_producto;
                    if (p.url_img.Trim().Equals(""))
                    {
                        pictureBoxImagen.Image = GUI.Properties.Resources.icon320x320;

                    }
                    else
                    {
                        pictureBoxImagen.ImageLocation = p.url_img;
                    }
                    lblidproducto.Text = Convert.ToString(p.id_producto);

                    txtNombre.ReadOnly = true;
                    txtDescripcion.ReadOnly = true;
                    txtPrecio.ReadOnly = true;
                    btnVender.Enabled = false;
                    btnModificar.Enabled = false;
                }
            
            
            }else{
                MessageBox.Show("Error no se pudo obtener la información de la Base de Datos");
            }
        
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarForm();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            republicarProducto();
        }

        private void republicarProducto() {
            Producto p = new Producto();
            p.id_producto = Convert.ToInt32(lblidproducto.Text);
            p.rut_usuario = Convert.ToInt32(lblUsuario.Text);
            p.nombre_producto = txtNombre.Text;
            p.descripcion_producto = txtDescripcion.Text;
            p.precio_inicial = Convert.ToInt32(txtPrecio.Text);
            p.url_img = pictureBoxImagen.ImageLocation;
            p.fecha_inscripcion = Convert.ToString(DateTime.Now);
            p.estado = "Disponible";

            ProductosBLL.ActualizaProducto(p);

            MessageBox.Show("Producto republicado satisfactoriamente");
        
        }

        private void GrillaProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
