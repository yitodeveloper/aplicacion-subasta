﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using BLL;

namespace GUI
{
    public partial class MisCompras : Form
    {
        public MisCompras()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void UltimaSubastaProducto(int rut)
        {
            List<Producto> compras = new List<Producto>();
            List<Producto> listado = ProductosBLL.CargarProductosVendidos();

            foreach (Producto p in listado)
            {
                int idproducto = p.id_producto;
                int preciofinal = Convert.ToInt32(p.precio_final);
                Subastas s = SubastaBLL.CargarUltimaSubasta(idproducto, preciofinal);
                //MessageBox.Show("Comparando : rut subasta "+s.rut_usuario+" v/s usuario "+rut );
                if (s.rut_usuario == rut) {

                    compras.Add(p);
                } 

            }

            DataTable tabla = new DataTable();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("DESCRIPCIÓN");
            tabla.Columns.Add("MONTO PAGADO");

            if (compras.Count > 0)
            {

                foreach (Producto p in compras)
                {

                    tabla.Rows.Add(p.id_producto, p.nombre_producto, p.descripcion_producto, p.precio_final);
                }

            }

            try
            {
                GrillaProductos.DataSource = tabla;

                GrillaProductos.Columns[0].Width = 50;
                GrillaProductos.Columns[1].Width = 350;
                GrillaProductos.Columns[2].Width = 350;
            }
            catch (NullReferenceException ne)
            {

            }


            
        

        }
    }
}
