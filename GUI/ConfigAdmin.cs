﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Entities;
using BLL;

namespace GUI
{
    public partial class ConfigAdmin : Form
    {
        public ConfigAdmin()
        {
            InitializeComponent();
            leerTXT();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void ConfigAdmin_Load(object sender, EventArgs e)
        {

        }

        private void leerTXT() { 
            StreamReader ficheroLectura = new StreamReader(@"C:\Users\Rodrigo\documents\visual studio 2010\Projects\SubastAPP\GUI\minutos.txt");
            int lineaFicheroL = Convert.ToInt32(ficheroLectura.ReadLine());
            cboMinutos.SelectedIndex = lineaFicheroL ;
            ficheroLectura.Close();
        
        }

        private void GuargarTxt() { 
            
            StreamWriter ficheroEscritura = new StreamWriter(@"C:\Users\Rodrigo\documents\visual studio 2010\Projects\SubastAPP\GUI\minutos.txt");
            ficheroEscritura.WriteLine(Convert.ToInt32(cboMinutos.SelectedItem)-1);
            ficheroEscritura.Close();
            MessageBox.Show("Cambios realizados correctamente : " + cboMinutos.SelectedItem);
        
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            GuargarTxt();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (ValidadFormulario())
            {
                RegistrarUsuario();

            }
            else {
                MessageBox.Show("Existen campos sin completar, favor de verificar");
            }
        }

        private void RegistrarUsuario() {

            Usuarios u = new Usuarios();

            u.rut_usuario = Convert.ToInt32(txtRut.Text);
            u.dv_usuario = txtDv.Text;
            u.nombre_usuario = txtNombre.Text;
            u.apellido_usuario = txtApellido.Text;
            u.password = txtpass.Text;
            u.tipo_usuario = Convert.ToString(cboTipoUsuario.SelectedItem);

            UsuarioBLL.RegistrarUsuario(u);

            MessageBox.Show("Usuario registrado correctamente");
            LimpiarForm();

        }

        private Boolean ValidadFormulario() {

            if (
                txtRut.Text != "" &&
                txtDv.Text != "" &&
                txtNombre.Text != "" &&
                txtApellido.Text != "" &&
                txtpass.Text != "" &&
                cboTipoUsuario.SelectedIndex > -1
                )
            {
                return true;
            }
            else {
                return false;
            }
        }

        private void LimpiarForm() { 
        
                txtRut.Text = "" ;
                txtDv.Text = "" ;
                txtNombre.Text = "" ;
                txtApellido.Text = "";
                txtpass.Text = "";
                cboTipoUsuario.SelectedIndex = -1;
        }
    }
}
