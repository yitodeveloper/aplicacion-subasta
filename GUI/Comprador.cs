﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using BLL;
using System.IO;
using System.Media;

namespace GUI
{
    public partial class Comprador : Form
    {
        public Comprador()
        {
            InitializeComponent();
            ListarProductosDisponibles();
        }


        public int query = 0;


        private void Comprador_Load(object sender, EventArgs e)
        {

        }

        private void ListarProductosDisponibles()
        {
            
            int filaseleccionada = 0;
            try
            {
                filaseleccionada = GrillaProductos.SelectedRows[0].Index;
            }
            catch (Exception ex)
            {

            }

            DataTable tabla = new DataTable();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("ULTIMA OFERTA ($)");
            tabla.Columns.Add("TIEMPO RESTANTE");

            List<Producto> resultado;

            if (query == 0)
            {

               resultado = ProductosBLL.CargarProductosDisponibles();

            }
            else {
                 String q = txtBusqueda.Text;
                 resultado = ProductosBLL.CargarProductosDisponiblesFiltro(q);
                
            }
            

            if (resultado.Count > 0)
            {

                foreach (Producto p in resultado)
                {

                    tabla.Rows.Add(p.id_producto, p.nombre_producto, p.precio_final, Cronometro(p.fecha_inscripcion, p));
                }

            }

            GrillaProductos.DataSource = tabla;
            try
            {
                GrillaProductos.CurrentRow.Selected = false;
                GrillaProductos.Rows[filaseleccionada].Selected = true;
               
            }
            catch (Exception ne)
            {

            }

            GrillaProductos.Columns[0].Width = 80;
            GrillaProductos.Columns[1].Width = 200;
            GrillaProductos.Columns[2].Width = 100;
            GrillaProductos.Columns[3].Width = 150;
        }

        private String Cronometro(String tiempo, Producto p)
        {

            StreamReader ficheroLectura = new StreamReader(@"C:\Users\Rodrigo\documents\visual studio 2010\Projects\SubastAPP\GUI\minutos.txt");
            int lineaFicheroL = Convert.ToInt32(ficheroLectura.ReadLine());
            ficheroLectura.Close();


            int minutos = lineaFicheroL;
            int segundos = 60;
            DateTime fechaAntiguo = Convert.ToDateTime(tiempo);
            DateTime fechaNueva = DateTime.Now;

            TimeSpan diferencia = fechaNueva - fechaAntiguo;

            int dfMinutos = minutos - diferencia.Minutes;
            int dfSegundos = segundos - diferencia.Seconds;



            if (dfMinutos <= 0)
            {

                if (dfSegundos == 5)
                {
                    SoundPlayer simpleSound = new SoundPlayer(@"c:\sonidos\tiempo3.wav");
                    simpleSound.Play();
                }
                return (Convert.ToString(dfSegundos) + " segundos");
                

            }
            else
            {
                return (Convert.ToString(dfMinutos) + ":" + Convert.ToString(dfSegundos));

            }



        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            ListarProductosDisponibles();
        }

        private void GrillaProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GrillaProductos_MouseClick(object sender, MouseEventArgs e)
        {
            CargarFormulario();
        }

        private void CargarFormulario()
        {
            try
            {
                int idproducto = Convert.ToInt32(GrillaProductos.SelectedRows[0].Cells[0].Value);

                Producto p = BLL.ProductosBLL.ConsultarProducto(idproducto);

                if (p != null)
                {

                    if (p.estado.Trim().Equals("Disponible"))
                    {

                        lblPrecioInicial.Text = "Precio Inicial : "+Convert.ToString(p.precio_inicial);
                        lblNombre.Text = "Nombre : "+p.nombre_producto;
                        lblDescripcion.Text = "Descripción : "+p.descripcion_producto;
                        if (p.precio_final == null)
                        {

                            lblPrecioActual.Text = Convert.ToString(p.precio_inicial);
                        }
                        else {
                            lblPrecioActual.Text = Convert.ToString(p.precio_final);
                        }
                        
                        if (p.url_img.Trim().Equals(""))
                        {
                            pictureBoxImagen.Image = GUI.Properties.Resources.icon320x320;

                        }
                        else
                        {
                            pictureBoxImagen.ImageLocation = p.url_img;
                        }
                        lblidproducto.Text = Convert.ToString(p.id_producto);

                        txtMontoPuja.ReadOnly = false;

                        
                        timer2.Enabled = true;

                    }
                    else
                    {
                        
                        txtMontoPuja.ReadOnly = true;
                        btnPujar.Enabled = false;
                        timer2.Enabled = false;

                        

                        MessageBox.Show("Tiempo de subasta a expirado, no se puede pujar");
                    }


                }
                else
                {
                    MessageBox.Show("Error no se pudo obtener la información de la Base de Datos");
                }
            }
            catch (Exception ep) { 
            
            }
        }

        private void ActualizarDatos()
        {
            /*try
            {*/
                int idproducto = Convert.ToInt32(lblidproducto.Text);

                Producto p = BLL.ProductosBLL.ConsultarProducto(idproducto);

                if (p != null)
                {

                    if (p.estado.Trim().Equals("Disponible"))
                    {

                        lblTiempo.Text = "Tiempo Restante : " + Cronometro(p.fecha_inscripcion, p);

                        if (p.precio_final == null)
                        {

                            lblPrecioActual.Text = Convert.ToString(p.precio_inicial);
                        }
                        else
                        {
                            lblPrecioActual.Text = Convert.ToString(p.precio_final);
                        }

                        txtMontoPuja.ReadOnly = false;


                    }
                    else
                    {
                        
                        txtMontoPuja.ReadOnly = true;
                        btnPujar.Enabled = false;
                        timer2.Enabled = false;
                        lblTiempo.Text = "Tiempo Expirado";
                        //MessageBox.Show("Tiempo de subasta a expirado, no se puede pujar");

                        if (UltimaSubastaProducto().rut_usuario == Convert.ToInt32(lblUsuario.Text))
                        {
                            lblTiempo.Text = "Felicitaciones haz adquirido el producto";
                            SoundPlayer simpleSound = new SoundPlayer(@"c:\sonidos\ganar.wav");
                            simpleSound.Play();
                         

                        }
                    }


                }
                else
                {
                    MessageBox.Show("Error no se pudo obtener la información de la Base de Datos");
                }
            /*}
            catch (InvalidCastException ep)
            {

            }*/
        }

        private Subastas UltimaSubastaProducto() {

            Subastas s = SubastaBLL.CargarUltimaSubasta(Convert.ToInt32(lblidproducto.Text),Convert.ToInt32(lblPrecioActual.Text));

            return s;
        
        }


        private void ListarProductosAdquiridos() {
        
        }


        private void txtMontoPuja_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int montopuja = Convert.ToInt32(txtMontoPuja.Text);
                int montoactual = Convert.ToInt32(lblPrecioActual.Text);
                if (montopuja > montoactual)
                {
                    btnPujar.Enabled = true;

                }
                else
                {
                    btnPujar.Enabled = false;

                }
            }
            catch (Exception ex) { 
            
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            
            ActualizarDatos();
        }

        public void AsignarUsuario(String user)
        {
            lblUsuario.Text = user;
            
        }

        private void btnPujar_Click(object sender, EventArgs e)
        {
            pujarMonto();
        }

        private void pujarMonto() {
        
            int idproducto = Convert.ToInt32(lblidproducto.Text);

                Producto p = BLL.ProductosBLL.ConsultarProducto(idproducto);

                if (p != null)
                {

                    if (p.estado.Trim().Equals("Disponible"))
                    {
                        Subastas s = new Subastas();

                        s.id_producto = idproducto;
                        s.fecha_subasta = Convert.ToString(DateTime.Now);
                        s.monto_subastado = Convert.ToInt32(txtMontoPuja.Text);
                        s.rut_usuario = Convert.ToInt32(lblUsuario.Text.Trim());

                        if(SubastaBLL.SubastarProducto(s).id_subasta >0){

                            btnPujar.Enabled = false;
                            SoundPlayer simpleSound = new SoundPlayer(@"c:\sonidos\boton2.wav");
                            simpleSound.Play();
                            txtMontoPuja.Focus();
                        }
                    }

                }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            query = 1;
            ListarProductosDisponibles();
        }

        private void txtMontoPuja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
                {
                    e.Handled = false;
                }
                else
                {
                    //el resto de teclas pulsadas se desactivan
                    e.Handled = true;
                } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnMisCompras_Click(object sender, EventArgs e)
        {
            MisCompras n = new MisCompras();
            n.Show();
            n.UltimaSubastaProducto(Convert.ToInt32(lblUsuario.Text));
        }


    }
}
