﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using BLL;
using System.IO;

namespace GUI
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void Reloj1_Tick(object sender, EventArgs e)
        {
            RelojHora();
        }

        private void RelojHora () {

            lblHoraSistema.Text = Convert.ToString(DateTime.Now);
            ListarProductosDisponibles();
        
        }

        private void ListarProductosDisponibles() {
            int filaseleccionada = 0;
            try
            {
                filaseleccionada = GrillaProductos.SelectedRows[0].Index;
            }
            catch (Exception ex) { 
            
            }
            
            DataTable tabla = new DataTable();
            tabla.Columns.Add("ID");
            tabla.Columns.Add("PRODUCTO");
            tabla.Columns.Add("DESCRIPCIÓN");
            tabla.Columns.Add("ULTIMA OFERTA ($)");
            tabla.Columns.Add("TIEMPO RESTANTE");

            List<Producto> resultado = ProductosBLL.CargarProductosDisponibles();

            if (resultado.Count > 0) { 
            
                foreach(Producto p in resultado){
                    
                    tabla.Rows.Add(p.id_producto, p.nombre_producto,p.descripcion_producto,p.precio_final,Cronometro(p.fecha_inscripcion, p));
                }
            
            }

            GrillaProductos.DataSource = tabla;
            try
            {
                GrillaProductos.CurrentRow.Selected = false;
            }
            catch (NullReferenceException ne) { 
            
            }
            GrillaProductos.Rows[filaseleccionada].Selected = true;
            GrillaProductos.Columns[0].Width = 50;
            GrillaProductos.Columns[1].Width = 350;
            GrillaProductos.Columns[2].Width = 350;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (ValidarForm())
            {
                login();
            }
            else {
                MessageBox.Show("Error: Campos de datos vacios");
            }
        }


        private void login() {

            int rut = Convert.ToInt32(txtRut.Text);
            String pass = txtPass.Text;

            if (UsuarioBLL.login(rut, pass))
            {
                
                String tipouser = Convert.ToString(BLL.UsuarioBLL.ConsultarTipoUsuario(rut).Trim());

                if(tipouser.Equals("Vendedor"))
                {
                    Vendedor v = new Vendedor();
                    v.Show();
                    v.AsignarUsuario(Convert.ToString(rut));
                    limpiarForm();
                }else{

                    Comprador c = new Comprador();
                    c.Show();
                    c.AsignarUsuario(Convert.ToString(rut));
                    limpiarForm();
                }

            }
            else {
                MessageBox.Show("Error : Rut o Contraseña ingresada incorrectamente");
            }
        
        }

        private void limpiarForm() {
            txtDv.Text = "";
            txtPass.Text = "";
            txtRut.Text = "";
        }


        private Boolean ValidarForm() {

            if (
                txtRut.Text != "" &&
                txtDv.Text != "" &&
                txtPass.Text != ""
                )
            {

                return true;
            }
            else {
                return false;
            }
        
        }

        private String Cronometro(String tiempo, Producto p) {

            StreamReader ficheroLectura = new StreamReader(@"C:\Users\Rodrigo\documents\visual studio 2010\Projects\SubastAPP\GUI\minutos.txt");
            int lineaFicheroL = Convert.ToInt32(ficheroLectura.ReadLine());
            ficheroLectura.Close();


            int minutos = lineaFicheroL;
            int segundos = 60;
            DateTime fechaAntiguo = Convert.ToDateTime(tiempo);
            DateTime fechaNueva = DateTime.Now;

            TimeSpan diferencia = fechaNueva - fechaAntiguo;

            int dfMinutos = minutos - diferencia.Minutes;
            int dfSegundos = segundos - diferencia.Seconds;

            

            if (dfMinutos <= 0)
            {
                if (dfSegundos <= 1)
                {

                    FinalizarSubasta(p);
                    
                    
                }
                return (Convert.ToString(dfSegundos)+" segundos");

                
            }
            else {
                return (Convert.ToString(dfMinutos) + ":" + Convert.ToString(dfSegundos)); 
            
            }

            
        
        }

        private void FinalizarSubasta(Producto p) {

            SubastaBLL.FinalizarSubasta(p);
        
        }

        private void SeguimientoSubasta(Producto p) { 
        
        
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtRut_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRut_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
                {
                    e.Handled = false;
                }
                else
                {
                    //el resto de teclas pulsadas se desactivan
                    e.Handled = true;
                } 
        }

        private void txtDv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
                if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
                {
                    e.Handled = false;
                }
                else
                {
                    //el resto de teclas pulsadas se desactivan
                    e.Handled = true;
                } 
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void pictureBoxConfig_Click(object sender, EventArgs e)
        {
            new AdminAccess().Show();
        }
    }
}
