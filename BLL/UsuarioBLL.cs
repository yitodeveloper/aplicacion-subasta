﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DAL;

namespace BLL
{
    public class UsuarioBLL
    {
        public static Boolean login(int rut, string pass) {

            
            Usuarios u = DAL.UsuarioDAL.ConsultarUsuario(rut);

            if (u != null)
            {
                
                if (Convert.ToString(u.password.Trim()).Equals( pass))
                {
                    return true;
                }
                else {

                    return false;
                }

            }
            else {
                return false;
            }
        
        }

        public static String ConsultarTipoUsuario(int rut) {

            Usuarios u = DAL.UsuarioDAL.ConsultarUsuario(rut);

            return u.tipo_usuario;
        
        }

        public static Usuarios RegistrarUsuario(Usuarios us) { 
        
            return UsuarioDAL.RegistrarUsuario(us);
        }
    }
}
