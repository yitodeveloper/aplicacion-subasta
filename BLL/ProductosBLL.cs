﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DAL;

namespace BLL
{
    public class ProductosBLL
    {
        public static List<Producto> CargarProductosDisponibles()
        {
            return ProductoDAL.CargarProductosDisponibles();
        }

        public static List<Producto> CargarProductosVendidos()
        {
            return ProductoDAL.CargarProductosVendidos();
        }

        public static List<Producto> CargarProductosDisponiblesFiltro(String q)
        {
            return ProductoDAL.CargarProductosDisponiblesFiltro(q);
        }

        public static List<Producto> CargarProductosUsuario(int rut)
        {
            return ProductoDAL.CargarProductosUsuario(rut);
        }

        public static Producto VenderProducto(Producto p)
        {

            return DAL.ProductoDAL.RegistrarProducto(p);
        }

        public static Producto ActualizarEstadoProducto(Producto p, String estado) {
            return DAL.ProductoDAL.ActualizarEstadoProducto(p, estado);
        }

        public static Producto ConsultarProducto(int idproducto) {
            return ProductoDAL.ConsultarProducto(idproducto);
        }

        public static Producto ActualizaProducto(Producto p)
        {
            return DAL.ProductoDAL.RepublicarProducto(p);
        }
    }
}
