﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DAL;

namespace BLL
{
    public class SubastaBLL
    {
        public static int FinalizarSubasta(Producto p) {

            if (p.precio_final != null)
            {

                ProductosBLL.ActualizarEstadoProducto(p, "Vendido");

            }
            else {

                ProductosBLL.ActualizarEstadoProducto(p, "Finalizado");
            
            }

            return 1;
        }

        public static Subastas SubastarProducto(Subastas s) {

            Subastas n = DAL.SubastaDAL.SubastarProducto(s);

            Producto p = DAL.ProductoDAL.ConsultarProducto(Convert.ToInt32(n.id_producto));

            p.precio_final = s.monto_subastado;

            DAL.ProductoDAL.UpdateProducto(p);

            return n;
        
        }

        public static Subastas CargarUltimaSubasta(int idproducto, int precioactual) {

            return SubastaDAL.CargarUltimaSubasta(idproducto, precioactual);
        
        }
    }
}
